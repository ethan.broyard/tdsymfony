# TDSymphony



## Auteurs

BROYARD Ethan BOISSAY Gatien BOUFFARD Léo

## Languages

Symfony,php,twig

## Installation

1- executer composer install dans votre terminal

2- Faite php bin/console make:migration

3- Faite php bin/console doctrine:migrations:migrate

4- Faite php bin/console doctrine:fixtures:load

Il peut y avoir une erreur donc n'hesitez pas à relancer la commande des fixtures

## Routes disponibles

/musique 
:permet d'obtenir un tableau contenant les musiques ainsi que tous ce qui concerne les musiques

/autheur
:permet d'obtenir un tableau contenant les artists ainsi que tous ce qui concerne les artistes notamment les ajouts etc

/album
:permet d'obtenir un tableau contenant les albums ainsi que tous ce qui concerne les albums notamment les musiques des albums etc

ps: il n'y a qu'un nombre limité de musique par album pour des questions de chargement de données etc 