<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230406154649 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE album (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, title VARCHAR(255) NOT NULL, cover VARCHAR(255) DEFAULT NULL, nb_track INTEGER NOT NULL, release_date VARCHAR(255) DEFAULT NULL)');
        $this->addSql('CREATE TABLE album_musique (album_id INTEGER NOT NULL, musique_id INTEGER NOT NULL, PRIMARY KEY(album_id, musique_id), CONSTRAINT FK_BBCA0BAE1137ABCF FOREIGN KEY (album_id) REFERENCES album (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE, CONSTRAINT FK_BBCA0BAE25E254A1 FOREIGN KEY (musique_id) REFERENCES musique (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('CREATE INDEX IDX_BBCA0BAE1137ABCF ON album_musique (album_id)');
        $this->addSql('CREATE INDEX IDX_BBCA0BAE25E254A1 ON album_musique (musique_id)');
        $this->addSql('CREATE TABLE album_autheur (album_id INTEGER NOT NULL, autheur_id INTEGER NOT NULL, PRIMARY KEY(album_id, autheur_id), CONSTRAINT FK_EE0A54C41137ABCF FOREIGN KEY (album_id) REFERENCES album (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE, CONSTRAINT FK_EE0A54C4C6E59929 FOREIGN KEY (autheur_id) REFERENCES autheur (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('CREATE INDEX IDX_EE0A54C41137ABCF ON album_autheur (album_id)');
        $this->addSql('CREATE INDEX IDX_EE0A54C4C6E59929 ON album_autheur (autheur_id)');
        $this->addSql('CREATE TABLE autheur (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, nom VARCHAR(255) NOT NULL, prenom VARCHAR(255) NOT NULL)');
        $this->addSql('CREATE TABLE musique (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, title VARCHAR(255) NOT NULL, release_year VARCHAR(255) NOT NULL, image VARCHAR(255) DEFAULT NULL, description VARCHAR(255) DEFAULT NULL, son VARCHAR(500) DEFAULT NULL, duration VARCHAR(255) DEFAULT NULL)');
        $this->addSql('CREATE TABLE musique_autheur (musique_id INTEGER NOT NULL, autheur_id INTEGER NOT NULL, PRIMARY KEY(musique_id, autheur_id), CONSTRAINT FK_26659F3525E254A1 FOREIGN KEY (musique_id) REFERENCES musique (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE, CONSTRAINT FK_26659F35C6E59929 FOREIGN KEY (autheur_id) REFERENCES autheur (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('CREATE INDEX IDX_26659F3525E254A1 ON musique_autheur (musique_id)');
        $this->addSql('CREATE INDEX IDX_26659F35C6E59929 ON musique_autheur (autheur_id)');
        $this->addSql('CREATE TABLE messenger_messages (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, body CLOB NOT NULL, headers CLOB NOT NULL, queue_name VARCHAR(190) NOT NULL, created_at DATETIME NOT NULL, available_at DATETIME NOT NULL, delivered_at DATETIME DEFAULT NULL)');
        $this->addSql('CREATE INDEX IDX_75EA56E0FB7336F0 ON messenger_messages (queue_name)');
        $this->addSql('CREATE INDEX IDX_75EA56E0E3BD61CE ON messenger_messages (available_at)');
        $this->addSql('CREATE INDEX IDX_75EA56E016BA31DB ON messenger_messages (delivered_at)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE album');
        $this->addSql('DROP TABLE album_musique');
        $this->addSql('DROP TABLE album_autheur');
        $this->addSql('DROP TABLE autheur');
        $this->addSql('DROP TABLE musique');
        $this->addSql('DROP TABLE musique_autheur');
        $this->addSql('DROP TABLE messenger_messages');
    }
}
