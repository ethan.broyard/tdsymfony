<?php

namespace App\Controller;

use App\Entity\Autheur;
use App\Form\AutheurType;
use App\Repository\AutheurRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/autheur')]
class AutheurController extends AbstractController
{
    #[Route('/', name: 'app_autheur_index', methods: ['GET'])]
    public function index(AutheurRepository $autheurRepository): Response
    {
        return $this->render('autheur/index.html.twig', [
            'autheurs' => $autheurRepository->findAll(),
        ]);
    }

    #[Route('/new', name: 'app_autheur_new', methods: ['GET', 'POST'])]
    public function new(Request $request, AutheurRepository $autheurRepository): Response
    {
        $autheur = new Autheur();
        $form = $this->createForm(AutheurType::class, $autheur);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $autheurRepository->save($autheur, true);

            return $this->redirectToRoute('app_autheur_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('autheur/new.html.twig', [
            'autheur' => $autheur,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_autheur_show', methods: ['GET'])]
    public function show(Autheur $autheur): Response
    {
        return $this->render('autheur/show.html.twig', [
            'autheur' => $autheur,
        ]);
    }

    #[Route('/{id}/edit', name: 'app_autheur_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Autheur $autheur, AutheurRepository $autheurRepository): Response
    {
        $form = $this->createForm(AutheurType::class, $autheur);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $autheurRepository->save($autheur, true);

            return $this->redirectToRoute('app_autheur_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('autheur/edit.html.twig', [
            'autheur' => $autheur,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_autheur_delete', methods: ['POST'])]
    public function delete(Request $request, Autheur $autheur, AutheurRepository $autheurRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$autheur->getId(), $request->request->get('_token'))) {
            $autheurRepository->remove($autheur, true);
        }

        return $this->redirectToRoute('app_autheur_index', [], Response::HTTP_SEE_OTHER);
    }
}
