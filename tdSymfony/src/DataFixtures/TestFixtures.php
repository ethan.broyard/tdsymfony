<?php

namespace App\DataFixtures;
use App\Entity\Autheur;
use App\Entity\Musique;
use App\Entity\Album;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;
use Faker\Factory;

class TestFixtures extends Fixture
{
    private $client;
    // Injection de dépendance du client HTTP via le constructeur
    public function __construct(HttpClientInterface $client)
    {
        $this->client = $client;
    }

   

    public function load(ObjectManager $manager): void
    { 
        
        require_once 'vendor/autoload.php';

        $listAlbum=array();

        for ($i = 1; $i <= 200; $i++) {
            $response = $this->client->request('GET', "https://api.deezer.com/artist/$i");
            $artist_data = $response->toArray();
            if(isset($artist_data['error'])){
                continue;
            }
            $auteur = new Autheur();
            $auteur->setNom($artist_data['name']);
            $auteur->setPrenom($artist_data['name']);
            $musiques = $artist_data['tracklist'];
            
            $response = $this->client->request('GET', $musiques);
            $musiques_data = $response->toArray();
            $manager->persist($auteur);
            
           

            for($f =0;$f<= 20; $f++){
                if(isset($musiques_data['error'])){
                    continue;
                }
                // if(isset($musique_data['data'][$f]['id'])){
                //     continue;
                // }
            $musique_link=$musiques_data['data'][$f]['id'];
            $response = $this->client->request('GET', "https://api.deezer.com/track/$musique_link");
            $musique_data = $response->toArray();
            if(isset($musique_data['error'])){
                continue;
            }
            $musique = new Musique();
            $musique->setTitle($musique_data['title']);
            $musique->setReleaseYear($musique_data['release_date']);
            $musique->setImage($musique_data['album']['cover_big']);
            $musique->setDescription($musique_data['title']);
            $musique->addAutheur($auteur);
            $musique->setSon($musique_data['preview']);
            $musique->setDuration($musique_data['duration']);
            $manager->persist($musique);
            }
       

            // for($i=302127; $i<=302130;$i++){
            if(empty($musique_data['album'])){
                continue;
            }
            $id=$musique_data["album"]['id'];
            $response = $this->client->request('GET', "https://api.deezer.com/album/$id");
            $album_data = $response->toArray();
            if(isset($album_data['error'])){
                continue;
            }
            if(!in_array($album_data['id'],$listAlbum)){
                $album = new Album();
                $album->setTitle($album_data['title']);
                $album->setCover($album_data['cover']);
                $album->setNbTrack($album_data['nb_tracks']);
                $album->setReleaseDate($album_data['release_date']);
                $album->addMusiquelist($musique);
                $manager->persist($album);
            }
            else{
                $album->AddMusiqueList($musique);
            }
            $listAlbum[]=$album_data['id'];
             
            // }


}

$manager->flush();
}
}