<?php

namespace App\Entity;

use App\Repository\AlbumRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: AlbumRepository::class)]
class Album
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $title = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $cover = null;

    #[ORM\Column]
    private ?int $nb_track = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $release_date = null;

    #[ORM\ManyToMany(targetEntity: Musique::class, inversedBy: 'albums')]
    private Collection $musiquelist;

    #[ORM\ManyToMany(targetEntity: Autheur::class, inversedBy: 'albums')]
    private Collection $autheur;

    public function __construct()
    {
        $this->musiquelist = new ArrayCollection();
        $this->autheur = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getCover(): ?string
    {
        return $this->cover;
    }

    public function setCover(?string $cover): self
    {
        $this->cover = $cover;

        return $this;
    }

    public function getNbTrack(): ?int
    {
        return $this->nb_track;
    }

    public function setNbTrack(int $nb_track): self
    {
        $this->nb_track = $nb_track;

        return $this;
    }

    public function getReleaseDate(): ?string
    {
        return $this->release_date;
    }

    public function setReleaseDate(?string $release_date): self
    {
        $this->release_date = $release_date;

        return $this;
    }

    /**
     * @return Collection<int, Musique>
     */
    public function getMusiquelist(): Collection
    {
        return $this->musiquelist;
    }

    public function addMusiquelist(Musique $musiquelist): self
    {
        if (!$this->musiquelist->contains($musiquelist)) {
            $this->musiquelist->add($musiquelist);
        }

        return $this;
    }

    public function removeMusiquelist(Musique $musiquelist): self
    {
        $this->musiquelist->removeElement($musiquelist);

        return $this;
    }

    /**
     * @return Collection<int, Autheur>
     */
    public function getAutheur(): Collection
    {
        return $this->autheur;
    }

    public function addAutheur(Autheur $autheur): self
    {
        if (!$this->autheur->contains($autheur)) {
            $this->autheur->add($autheur);
        }

        return $this;
    }

    public function removeAutheur(Autheur $autheur): self
    {
        $this->autheur->removeElement($autheur);

        return $this;
    }
}
