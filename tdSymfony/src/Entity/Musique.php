<?php

namespace App\Entity;

use App\Repository\MusiqueRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: MusiqueRepository::class)]
class Musique
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $title = null;

    #[ORM\Column(length: 255)]
    private ?string $ReleaseYear = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $Image = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $Description = null;

    #[ORM\ManyToMany(targetEntity: Autheur::class, inversedBy: 'musiques')]
    private Collection $Autheur;

    #[ORM\Column(length: 500, nullable: true)]
    private ?string $son = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $duration = null;

    #[ORM\ManyToMany(targetEntity: Album::class, mappedBy: 'musiquelist')]
    private Collection $albums;

    public function __construct()
    {
        $this->Autheur = new ArrayCollection();
        $this->albums = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getReleaseYear(): ?string
    {
        return $this->ReleaseYear;
    }

    public function setReleaseYear(string $ReleaseYear): self
    {
        $this->ReleaseYear = $ReleaseYear;

        return $this;
    }

    public function getImage(): ?string
    {
        return $this->Image;
    }

    public function setImage(?string $Image): self
    {
        $this->Image = $Image;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->Description;
    }

    public function setDescription(?string $Description): self
    {
        $this->Description = $Description;

        return $this;
    }

    /**
     * @return Collection<int, Autheur>
     */
    public function getAutheur(): Collection
    {

        return $this->Autheur;
    }
    public function __toString() { return $this->Autheur; }

    public function addAutheur(Autheur $autheur): self
    {
        if (!$this->Autheur->contains($autheur)) {
            $this->Autheur->add($autheur);
        }

        return $this;
    }

    public function removeAutheur(Autheur $autheur): self
    {
        $this->Autheur->removeElement($autheur);

        return $this;
    }

    public function getSon(): ?string
    {
        return $this->son;
    }

    public function setSon(?string $son): self
    {
        $this->son = $son;

        return $this;
    }

    public function getDuration(): ?string
    {
        return $this->duration;
    }

    public function setDuration(?string $duration): self
    {
        $this->duration = $duration;

        return $this;
    }

    /**
     * @return Collection<int, Album>
     */
    public function getAlbums(): Collection
    {
        return $this->albums;
    }

    public function addAlbum(Album $album): self
    {
        if (!$this->albums->contains($album)) {
            $this->albums->add($album);
            $album->addMusiquelist($this);
        }

        return $this;
    }

    public function removeAlbum(Album $album): self
    {
        if ($this->albums->removeElement($album)) {
            $album->removeMusiquelist($this);
        }

        return $this;
    }
}
