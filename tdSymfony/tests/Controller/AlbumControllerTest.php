<?php

namespace App\Test\Controller;

use App\Entity\Album;
use App\Repository\AlbumRepository;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class AlbumControllerTest extends WebTestCase
{
    private KernelBrowser $client;
    private AlbumRepository $repository;
    private string $path = '/album/';

    protected function setUp(): void
    {
        $this->client = static::createClient();
        $this->repository = static::getContainer()->get('doctrine')->getRepository(Album::class);

        foreach ($this->repository->findAll() as $object) {
            $this->repository->remove($object, true);
        }
    }

    public function testIndex(): void
    {
        $crawler = $this->client->request('GET', $this->path);

        self::assertResponseStatusCodeSame(200);
        self::assertPageTitleContains('Album index');

        // Use the $crawler to perform additional assertions e.g.
        // self::assertSame('Some text on the page', $crawler->filter('.p')->first());
    }

    public function testNew(): void
    {
        $originalNumObjectsInRepository = count($this->repository->findAll());

        $this->markTestIncomplete();
        $this->client->request('GET', sprintf('%snew', $this->path));

        self::assertResponseStatusCodeSame(200);

        $this->client->submitForm('Save', [
            'album[title]' => 'Testing',
            'album[cover]' => 'Testing',
            'album[nb_track]' => 'Testing',
            'album[release_date]' => 'Testing',
            'album[musiquelist]' => 'Testing',
            'album[autheur]' => 'Testing',
        ]);

        self::assertResponseRedirects('/album/');

        self::assertSame($originalNumObjectsInRepository + 1, count($this->repository->findAll()));
    }

    public function testShow(): void
    {
        $this->markTestIncomplete();
        $fixture = new Album();
        $fixture->setTitle('My Title');
        $fixture->setCover('My Title');
        $fixture->setNb_track('My Title');
        $fixture->setRelease_date('My Title');
        $fixture->setMusiquelist('My Title');
        $fixture->setAutheur('My Title');

        $this->repository->save($fixture, true);

        $this->client->request('GET', sprintf('%s%s', $this->path, $fixture->getId()));

        self::assertResponseStatusCodeSame(200);
        self::assertPageTitleContains('Album');

        // Use assertions to check that the properties are properly displayed.
    }

    public function testEdit(): void
    {
        $this->markTestIncomplete();
        $fixture = new Album();
        $fixture->setTitle('My Title');
        $fixture->setCover('My Title');
        $fixture->setNb_track('My Title');
        $fixture->setRelease_date('My Title');
        $fixture->setMusiquelist('My Title');
        $fixture->setAutheur('My Title');

        $this->repository->save($fixture, true);

        $this->client->request('GET', sprintf('%s%s/edit', $this->path, $fixture->getId()));

        $this->client->submitForm('Update', [
            'album[title]' => 'Something New',
            'album[cover]' => 'Something New',
            'album[nb_track]' => 'Something New',
            'album[release_date]' => 'Something New',
            'album[musiquelist]' => 'Something New',
            'album[autheur]' => 'Something New',
        ]);

        self::assertResponseRedirects('/album/');

        $fixture = $this->repository->findAll();

        self::assertSame('Something New', $fixture[0]->getTitle());
        self::assertSame('Something New', $fixture[0]->getCover());
        self::assertSame('Something New', $fixture[0]->getNb_track());
        self::assertSame('Something New', $fixture[0]->getRelease_date());
        self::assertSame('Something New', $fixture[0]->getMusiquelist());
        self::assertSame('Something New', $fixture[0]->getAutheur());
    }

    public function testRemove(): void
    {
        $this->markTestIncomplete();

        $originalNumObjectsInRepository = count($this->repository->findAll());

        $fixture = new Album();
        $fixture->setTitle('My Title');
        $fixture->setCover('My Title');
        $fixture->setNb_track('My Title');
        $fixture->setRelease_date('My Title');
        $fixture->setMusiquelist('My Title');
        $fixture->setAutheur('My Title');

        $this->repository->save($fixture, true);

        self::assertSame($originalNumObjectsInRepository + 1, count($this->repository->findAll()));

        $this->client->request('GET', sprintf('%s%s', $this->path, $fixture->getId()));
        $this->client->submitForm('Delete');

        self::assertSame($originalNumObjectsInRepository, count($this->repository->findAll()));
        self::assertResponseRedirects('/album/');
    }
}
